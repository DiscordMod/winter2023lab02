public class PartThree
{
	public static void main(String[]args)
	{
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		System.out.println("Enter two values: ");
		int input1 = reader.nextInt();
		int input2 = reader.nextInt();
		
		Calculator calc = new Calculator();
		System.out.println(calc.add(input1, input2));
		
		System.out.println(calc.substract(input1, input2));
		
		System.out.println(Calculator.multiply(input1, input2));
		
		System.out.println(Calculator.divide(input1, input2));
	}
}