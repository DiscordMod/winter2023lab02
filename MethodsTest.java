public class MethodsTest
{
	public static void main(String[]args)
	{
		int x = 5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		methodTwoInputNoReturn(4, 6.9);
		int z = methodNoInputReturnInt();
		System.out.println(z);
		double squareRoot = sumSquareRoot(9, 5);
		System.out.println(squareRoot);
		
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		System.out.println(SecondClass.addOne(50));
		
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	public static void methodNoInputNoReturn()
	{
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
	}
	public static void methodOneInputNoReturn(int turtle)
	{
		turtle = turtle - 5;
		System.out.println("Inside the method one input no return: " + turtle);
	}
	public static void methodTwoInputNoReturn(int soup, double spoon)
	{
		System.out.println("the first value is " + soup + " and " + "the second value " + spoon);
	}
	public static int methodNoInputReturnInt()
	{
		return 5;
	}
	public static double sumSquareRoot(int macaroni, int pot)
	{
		double dinner = Math.sqrt(macaroni + pot);
		return dinner;
	}
} 